<?php
 
/*
 * DataTables example server-side processing script.
 *
 * Please note that this script is intentionally extremely simply to show how
 * server-side processing can be implemented, and probably shouldn't be used as
 * the basis for a large complex system. It is suitable for simple use cases as
 * for learning.
 *
 * See http://datatables.net/usage/server-side for full details on the server-
 * side processing requirements of DataTables.
 *
 * @license MIT - http://datatables.net/license_mit
 */
 
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * Easy set variables
 */
$currDir = dirname(__FILE__);
include("$currDir/defaultLang.php");
include("$currDir/language.php");
include("$currDir/lib.php");
$mi = getMemberInfo();

$perm=getTablePermissions('Piarco');
if(!$perm[0]){
	echo error_message($Translation['tableAccessDenied'], false);
	echo '<script>setTimeout("window.location=\'index.php?signOut=1\'", 2000);</script>';
	exit;
}

// DB table to use
$table = 'Piarco';
 
// Table's primary key
$primaryKey = 'ID';
 
// Array of database columns which should be read and sent back to DataTables.
// The `db` parameter represents the column name in the database, while the `dt`
// parameter represents the DataTables column identifier. In this case simple
// indexes
$columns = array(
    array( 'db' => 'Year', 'dt' => 0 ),
    array( 'db' => 'Month',  'dt' => 1 ),
    array( 'db' => 'Day',   'dt' => 2 ),
    array( 'db' => 'PRECIP', 'dt' => 3 ),
    array( 'db' => 'TEMPMIN', 'dt' => 4 ),
    array( 'db' => 'TEMPMAX', 'dt' => 5 )
);
 
// SQL server connection information
$sql_details = array(
    'user' => 'cli_user',
    'pass' => 'mer500bs.',
    'db'   => 'cli_dailydata',
    'host' => 'localhost'
);
 
 
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * If you just want to use the basic configuration for DataTables with PHP
 * server-side, there is no need to edit below this line.
 */
 
require( 'ssp.class.php' );
 
echo json_encode(
    SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns )
);