<?php
	$dbServer = 'localhost';
	$dbUsername = 'cli_user';
	$dbPassword = 'mer500bs.';
	$dbDatabase = 'cli_dailydata';

	$adminConfig = array(
		'adminUsername' => "climate",
		'adminPassword' => "b32dbfac6d40bf7c10a534fae3d9d9da",
		'notifyAdminNewMembers' => "2",
		'defaultSignUp' => "1",
		'anonymousGroup' => "anonymous",
		'anonymousMember' => "guest",
		'groupsPerPage' => "10",
		'membersPerPage' => "10",
		'recordsPerPage' => "10",
		'custom1' => "Full Name",
		'custom2' => "Organization",
		'custom3' => "Email Address",
		'custom4' => "Use of Data?",
		'MySQLDateFormat' => "%d/%m/%Y",
		'PHPDateFormat' => "j/n/Y",
		'PHPDateTimeFormat' => "d/m/Y, h:i a",
		'senderName' => "TTMS Climate management",
		'senderEmail' => "jerome_ramirez@hotmail.com",
		'approvalSubject' => "Your membership is now approved",
		'approvalMessage' => "Dear member,\r\n\r\nYour membership is now approved by the admin. You can log in to your account here:\r\nhttp://190.58.130.230/climate_data\r\n\r\nRegards,\r\nAdmin",
		'hide_twitter_feed' => ""
	);